from django.http import HttpResponsePermanentRedirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from py_scripts.joke import Joke
from py_scripts.store_chat import StoreChat, StoreURLS
from django.template import RequestContext
import json
from .models import Document
from forms import DocumentForm
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import requests
import pyimgur
import instagram_filters
from instagram_filters.filters import *

import shutil
import inspect
import os
# Create your views here.


j = Joke()
sc = StoreChat()
su = StoreURLS()

authorizationToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Imt1c2hhbHZ5YXNrdkBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InZjc19wYXJ0bmVyX2lkIjoiU3R1ZGVudEhhY2thdGhvbi03NTEyMWViYy0wYzVhLTQzM2UtOWViMi0wYTZkMzY0NjhhNzQifSwiaXNzIjoiaHR0cHM6Ly9jaW1wcmVzcy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg4OTk4MmQ1MDM1MWQzMmZmMzNjYjg5IiwiYXVkIjoiNEd0a3hKaHowVTFiZGdnSE1kYXlTQXkwNUlWNE1FRFYiLCJleHAiOjE0ODU2OTQ0NzgsImlhdCI6MTQ4NTY1ODQ3OH0.VqSvWASv_Z2lRFHHADac1hQkF083GeUt3ecbk35N71Q"


@csrf_exempt
def index(request):
	print request.path
	obj_item1 = str(request.path).split("/")
	obj_item = [each for each in obj_item1 if each is not ''][-1]
	print obj_item
	obj_item = obj_item.lower()
	print "Obj Item is " , obj_item
	context_dict = {}
	context_dict = return_context(obj_item)
	print "request method is :" , request
	print "j method " , j.call_method

	# if request.method == 'POST':
	# 	form = DocumentForm(request.POST, request.FILES)
	# 	print request.FILES
	# 	myfile = request.FILES['image']
	# 	fs = FileSystemStorage( )
	# 	filename = fs.save(myfile.name, myfile)
	# 	uploaded_file_url = fs.url(filename)
	# 	print "uploaded url is : ", uploaded_file_url
	# 	if form.is_valid():
	# 		# instance = Document(image=request.FILES['file'])
	# 		# instance.save()
	# 		form.save()
	# 		CLIENT_ID = "b5269faedb893ff"
	# 		PATH = settings.MEDIA_ROOT +"/" + filename
	# 		su.list_paths = applyAll(PATH)
	# 		im = pyimgur.Imgur(CLIENT_ID)
	# 		su.imgur_paths= []
	# 		for each_path in list_paths:
	# 			uploaded_image = im.upload_image(each_path, title="Uploaded with PyImgur")
	# 			print(uploaded_image.title)
	# 			print(uploaded_image.link)
	# 			print(uploaded_image.size)
	# 			print(uploaded_image.type)
	# 			serverurl = uploaded_image.link
	# 			retval = test_image_url(serverurl)
	# 			su.imgur_paths.append((serverurl, retval))

	# 		return HttpResponseRedirect('/cimpress_redhawk/')
	# else:
	# 	form = DocumentForm()


	# context_dict['form'] = form

	form = DocumentForm()
	context_dict['form'] = form

	if su.upload_im is not None:
		context_dict['upload_im'] = "http://127.0.0.1:8000" + settings.MEDIA_URL + su.upload_im

	if request.method == "GET" and j.call_method is not None:
		print "here inside get and jcall"
		context_dict = return_context(j.call_method)



	elif request.is_ajax() :
		input_text = request.POST.get("input_text")
		reply_j= j.execute_conversation(input_text, request)
		response = {}
		response['success'] = 1
		response['input_text'] = input_text
		response['response'] = reply_j
		########### store chat 
		
		sc.set_chat(input_text)
		sc.set_chat(reply_j)

		##############
		if j.call_method is None:
			response['only_response'] =1 
			return JsonResponse(response,  content_type="application/json")
		else:
			response['user_choice_about_items'] = 1
			context_dict = return_context(j.call_method)
			if j.call_method == "mug":
				su.skuglobal = "VIP-47735"
			else:
				su.skuglobal = "VIP-45554"
			j.call_method  = None
			print context_dict
			response['dict'] = json.dumps(context_dict)
			return JsonResponse(response, content_type='application/json')
			

	return render_to_response('cimpress_redhawk/index.html', context_dict)


@csrf_exempt
def post_form(request):

	if request.method == 'POST':
		form = DocumentForm(request.POST, request.FILES)
		print request.FILES
		myfile = request.FILES['image']
		fs = FileSystemStorage( )
		filename = fs.save(myfile.name, myfile)
		uploaded_file_url = fs.url(filename)
		print "uploaded url is : ", uploaded_file_url
		if form.is_valid():
			# instance = Document(image=request.FILES['file'])
			# instance.save()
			form.save()
			CLIENT_ID = "b5269faedb893ff"
			PATH = settings.MEDIA_ROOT +"/" + filename
			su.upload_im = filename
			print "Filename of uploaded image is : ", filename
			su.list_paths = applyAll(PATH)
			im = pyimgur.Imgur(CLIENT_ID)
			su.imgur_paths= []

			for each_path in su.list_paths:
				uploaded_image = im.upload_image(each_path, title="Uploaded with PyImgur")
				print(uploaded_image.title)
				print(uploaded_image.link)
				print(uploaded_image.size)
				print(uploaded_image.type)
				serverurl = uploaded_image.link
				retval = test_image_url(su.skuglobal, serverurl)
				su.imgur_paths.append((serverurl, retval))

			return HttpResponseRedirect('/cimpress_redhawk/')
	else:
		form = DocumentForm()

	# context_dict['form'] = form
	# return render_to_response('cimpress_redhawk/index.html', context_dict)

	return HttpResponseRedirect('/cimpress_redhawk/')



def return_context(obj_item):
	context_dict = {}
	context_dict['title'] = "Dashboard"
	if obj_item == 'mug':	
		context_dict['title'] = "Item Selection : Mug"
		print "MUG Selected"
		context_dict['item'] = 'mug'
		context_dict['im_list'] = [['http://127.0.0.1:8000/media/images/mug.jpg', 'VIP-47735'], ['http://127.0.0.1:8000/media/images/mug2.jpg','VIP-47738'], ['http://127.0.0.1:8000/media/images/mug3.png','VIP-47739']]

	if obj_item == "tshirt":
		print "Tshirt Selected"
		context_dict['title'] = "Item Selection : T Shirt"
		context_dict['item'] = 'tshirt'
		context_dict['im_list'] = [['http://127.0.0.1:8000/media/images/tshirt.jpg','VIP-45362'], ['http://127.0.0.1:8000/media/images/tshirt2.jpg','VIP-45364'], ['http://127.0.0.1:8000/media/images/tshirt3.jpg','VIP-45554']]

	if obj_item == "case":
		print "Phone Case Selected"
		context_dict['title'] = "Item Selection : Phone case"
		context_dict['item'] = ' phone case'
		context_dict['im_list'] = ['http://127.0.0.1:8000/media/images/case.jpg', 'http://127.0.0.1:8000/media/images/case2.jpg', 'http://127.0.0.1:8000/media/images/case3.jpg']

	if obj_item == "all":
		print "Show all products"
		context_dict['flag'] = 1
		context_dict['item'] = 'All products'
		context_dict['im_list'] = ['http://127.0.0.1:8000/media/images/mug.jpg', 'http://127.0.0.1:8000/media/images/mug2.jpg', 'http://127.0.0.1:8000/media/images/mug3.png',
		'http://127.0.0.1:8000/media/images/tshirt.jpg', 'http://127.0.0.1:8000/media/images/tshirt2.jpg', 'http://127.0.0.1:8000/media/images/tshirt3.jpg',
		'http://127.0.0.1:8000/media/images/case.jpg', 'http://127.0.0.1:8000/media/images/case2.jpg', 'http://127.0.0.1:8000/media/images/case3.jpg']
	

	return context_dict

def include(request):
	return render(request, 'cimpress_redhawk/include.html')


@csrf_exempt
def store_conversations(request):
	if request.method == "POST" or request.is_ajax():
		print "store chat ajax post"



def start_jwt():
	url = "https://cimpress.auth0.com/oauth/ro"
	headers = {'Content-Type': 'application/json'}
	data = {
				"client_id": "4GtkxJhz0U1bdggHMdaySAy05IV4MEDV",
				"username":"kushalvyaskv@gmail.com",
				"password":"pass@123",
				"connection":"default",
				"device":"none",
				"scope": "openid email app_metadata"
				}

	r = requests.post(url, data=json.dumps(data), headers=headers)
	r_json = r.json()
	auth_id = r_json['id_token']
	print "authentication id : " , auth_id


def fake_next(request):
	print "fake next request received"
	reply = j.execute_conversation("next", request)
	if request.is_ajax():
		return JsonResponse({'success':1, 'response':reply}, content_type='application/json')

	else:
		return JsonResponse({'success':0, 'response':reply}, content_type='application/json');


@csrf_exempt
def upload_pic(request):
	print request.POST

	return JsonResponse({'success':1}, content_type='application/json')




def test_image_url(skuglobal, imx):


	url = "https://api.cimpress.io/vcs/printapi/v2/documents/creators/url"
	# sku = "VIP-47735"
	# print encoded_image

	headers = {"Content-Type":"application/x-www-form-urlencoded",'Authorization' : authorizationToken}

	data=	{
	  "Sku": str(su.skuglobal),
	  "ImageUrls":[
	      imx
	  ]
	}

	r = requests.post(url, data=data, headers=headers)
	# r = requests.post(url, data=json.dumps(data), headers=headers)
	print r.status_code, r.text
	# session = requests.Session()
	# print session.post(url, headers=headers, data=str(data))
	return r.json()



def applyAll(filename):
	list_paths  = []
	d = "results"
	if not os.path.exists(d):
		os.makedirs(d)
		
	for name, obj in inspect.getmembers(instagram_filters.filters):
		if inspect.isclass(obj):
			# filename = os.path.join(d, name+".jpg")
			newfilename = ''.join(filename.split(".")[:-1]) + name + ''.join(filename.split(".")[-1])
			shutil.copyfile(filename, newfilename)
			list_paths.append(newfilename)
			f = obj(newfilename)
			f.apply()

	return list_paths


def scene_render(doc_url):
	print "docurl" , doc_url
	Sku = "VIP-47735"
	headers = {
		"Content-Type":"application/json",
		'Authorization':authorizationToken
	}

	url = "https://api.cimpress.io/vcs/printapi/v2/documents/scenes?width=800&documentReferenceUrl="+doc_url['DocumentReferenceUrl']

	r =  requests.get(url,headers=headers)
	data =  r.json()
	return data['Scenes'][0]['RenderingUrl']




@csrf_exempt
def renderit(request):
	print request.method
	
	if len(su.imgur_paths) > 5:
		docsli = su.imgur_paths[-5:]
	else:
		docsli = su.imgur_paths

	scene_urls = []
	for each in docsli:
		scene_urls.append( scene_render(each[1]))

	print scene_urls

	return JsonResponse({'success':1, 'urls':scene_urls}, content_type="application/json")
