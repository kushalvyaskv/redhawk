from __future__ import unicode_literals

from django.apps import AppConfig


class CimpressRedhawkConfig(AppConfig):
    name = 'cimpress_redhawk'
