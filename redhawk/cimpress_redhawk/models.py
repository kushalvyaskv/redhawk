from __future__ import unicode_literals

from django.db import models
from django.conf import settings

# Create your models here.
class Document(models.Model):
	image = models.FileField(upload_to=settings.MEDIA_ROOT)