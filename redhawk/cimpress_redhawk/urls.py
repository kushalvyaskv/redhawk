from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    # url(r'mug/', views.mug, name = 'mug'),
    # url(r'^$', views.index, name = 'index'),
    url(r'renderit', views.renderit, name = 'renderit'),
    url(r'fake_next', views.fake_next, name = 'fake_next'),
    url(r'upload_pic', views.upload_pic, name = 'upload_pic'),
    url(r'post_form', views.post_form, name = 'post_form'),
    url(r'', views.index, name = 'index'),
    # url(r'^all/', views.index, name = 'index'),
    # url(r'^tshirt/', views.index, name = 'index'),
    # url(r'^case/', views.index, name = 'index'),
    # url(r'^cards/', views.index, name = 'index'),
    # url(r'^include', views.include, name = 'include'),    
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
