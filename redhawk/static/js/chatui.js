var br_stack = [];
var br_stack_div;
var scroller;
var messages_div ;
var img_child_element;
var toggleflag;
var img_upload_filename ;
var cropper;
var new_img_element;
var finalSKU;

$(document).ready(function(){
	console.log("document is ready");
	br_stack_div = $("#br_space");
	scroller = $("#panel_messages");
	messages_div = $("#messages");
	init_chat_window()
	init_event_handlers();

	toggleflag = false;

	// for file
	// $(function() {
  //    $("input:file").change(function (){
  //      var fileName = $(this).val();
       // $(".filename").html(fileName);
  //    });
  // });
	$("#img_upload_button").change(function(){
		img_upload_filename = $(this).val();
		console.log("filename is :", img_upload_filename);

		upload_pic();

		// fake_next();
	});

});

function init_chat_window(){
	$("#input_chat_text").focus();
	console.log("init chat window");
	for(var i=0; i<12;i++){
		br_stack.push("<br>");
		br_stack_div.append(br_stack[i]);
	}
	execute_scroll_bottom();
	console.log(br_stack);
}

function execute_scroll_bottom(){
	scroller.scrollTop(scroller.prop("scrollHeight"));
}

function init_event_handlers(){
	$("#input_chat_text").keydown(function(event){
		if(event.keyCode == 13){
			console.log("enter is pressed");
			var input_text = $("#input_chat_text").val();
			console.log("input chat value is : " + input_text);
			post_to_view(input_text);
		}else{}
	});

	$("#render_button").click(handle_render);
}

function post_to_view(input_text){
	$('<div/>', {
		id: 'inp',
		class:'well well-md pull-right font_wt inp',
		align:'right',
		width: '30vh',
		text: input_text
	}).appendTo('#messages');
	
	execute_scroll_bottom();
	$("#input_chat_text").val("");

	$.ajax({
		url:"/cimpress_redhawk/",
		type:"POST",
		dataType:"json",
		data:{'input_text':input_text},
		success : handle_success_response_from_view,
		error: error_handler
	});
}

function  handle_success_response_from_view(data) {
	console.log(data);
	if(data.success){
		if(data.user_choice_about_items){
			var pd_c = $("#panel_display").children();
			for (var pdc_i =0 ; pdc_i < pd_c.length; pdc_i++){
				pd_c[pdc_i].remove();
			}
			var r =  JSON.parse(data.dict);
			console.log(r);
			// console.log(data.dict);
			$("#item_toggler").text(String(r.title));
			var iml = [];
			for(var i =0; i<r.im_list.length; i++){
				element = "<a href=\"#\" id=\""+ r.im_list[i][1] +"\" onclick=\"return register_env(this);\"><img src=\""+ String(r.im_list[i][0])+"\" width=\"100vh\" height=\"100vh\"></a> ";
				$("#panel_display").append(element);
			}
		}

		console.log("I am here");
		//append myinput
		response = JSON.parse(data.response);
		
		console.log("res" , response);
		var final_text = response[0];
		for(var ix=1;ix< response.length; ix++){
			final_text += response[ix];
			final_text += "  ";
			console.log(response[ix]);
		}
		console.log(final_text);
		// append response
		$('<div/>', {
			id: 'resp',
			class:'well well-md pull-left resp font_wt',
			align:'left',
			width:'30vh',
			text: final_text,
		}).appendTo('#messages');
		execute_scroll_bottom();
	}else{
	}

	

}
function error_handler(e){
	console.log("err");
	console.log(e);
}

function toggler(){
	if(toggleflag){
		toggleflag = false;
	}
	$("#show_render_big").children()[0].remove();
	$("#icon_display").fadeIn();
}


function register_env(x){
	finalSKU = x.id;
	console.log("Final SKU " , finalSKU);


}
function a_click(event){
	// console.log("a clicked");
	console.log(event);

	toggleflag = true;
	var current_a = $("#"+event.id);
	console.log(current_a.children());
	img_child_element = current_a.children()[0];
	console.log("img child element obtained",img_child_element);
	console.log(toggleflag);
	create_render_cropper_canvass();

}

function create_render_cropper_canvass(){
	if(toggleflag){
		$("#icon_display").fadeOut();
	}
	new_img_element = document.createElement("img");
	new_img_element.id = "for_cropper";
	new_img_element.src = img_child_element.src;
	new_img_element.style.height='50%';
	new_img_element.style.width='50%';
	new_img_element.object_fit = 'contain';
	$("#show_render_big").append(new_img_element);
	create_render_cropper(new_img_element);
	

}


function create_render_cropper(for_cropper){
	var dataX ;
    var dataY ;
    var dataHeight;
    var dataWidth ;
	cropper = new Cropper(for_cropper,{
		// aspectRatio: 4 / 3,
		data: {
			x: 480,
			y: 60,
			width: 640,
			height: 360
		},
		crop: function(e) {
			console.log(e.detail.x);
			console.log(e.detail.y);
			console.log(e.detail.width);
			console.log(e.detail.height);
			console.log(e.detail.rotate);
			console.log(e.detail.scaleX);
			console.log(e.detail.scaleY);
		},
		done: function(data){
			console.log(data);
			$dataX.val(Math.round(data.x));
			$dataY.val(Math.round(data.y));
			$dataHeight.val(Math.round(data.height));
			$dataWidth.val(Math.round(data.width));
		}

	});


	fake_next();
}


function img_upload_button_clicked(e){
	console.log("button clicked : img_upload_button");
}


function fake_next(){
	$.ajax({
		url:"/cimpress_redhawk/fake_next",
		data:{},
		dataType:'json',
		success:function(data){console.log(data);},
		error:function(e){console.log(e);},
	});

}


function upload_pic(){
	// this is the id of the form
	$("#img_upload_form").submit(function(e) {

	    var url = "/cimpress_redhawk/upload_pic"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#img_upload_filename").serialize(), // serializes the form's elements.
	           success: function(data){
	               alert(data); // show response from the php script.
	           },
	           error:function(data){
	           		console.log("eror");
	           },
	         });

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	
}


function handle_render(){
	console.log("here");
	var formdata = new FormData();
	formdata.append('Sku', finalSKU);
	$.ajax({
		url:'/cimpress_redhawk/renderit',
		type:"POST",
		dataType:'json',
		success:render_handler,
		error:render_error,
	});
	
}


function render_handler(data){
	console.log("yay");
	console.log(data);
	var _im_ids = "#ri"
	for (var i=1; i<4;i++){
		var tmp = _im_ids + String(i);
		var ix = $(tmp).attr('src', data.urls[String(i-1)]);
	}

	var rm = $("#render_modal_button");
	rm.trigger("click");
}

function render_error(e){
	console.log("error");
	console.log(e);
}