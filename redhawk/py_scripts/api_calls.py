import urllib
import requests
import base64
import json


def test_image_url():


	url = "https://api.cimpress.io/vcs/printapi/v2/documents/creators/url"
	sku = "VIP-47735"
	encoded_image = None
	with open("/home/kushal/Pictures/homeboys.jpg", "rb") as image_file:
	    encoded_image = base64.b64encode(image_file.read())

	# print encoded_image

	headers = {"Content-Type":"application/x-www-form-urlencoded",'Authorization' : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Imt1c2hhbHZ5YXNrdkBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InZjc19wYXJ0bmVyX2lkIjoiU3R1ZGVudEhhY2thdGhvbi03NTEyMWViYy0wYzVhLTQzM2UtOWViMi0wYTZkMzY0NjhhNzQifSwiaXNzIjoiaHR0cHM6Ly9jaW1wcmVzcy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg4OTk4MmQ1MDM1MWQzMmZmMzNjYjg5IiwiYXVkIjoiNEd0a3hKaHowVTFiZGdnSE1kYXlTQXkwNUlWNE1FRFYiLCJleHAiOjE0ODU2MzU1NjMsImlhdCI6MTQ4NTU5OTU2M30.JE0aNMBTUNfrTTPbcR8mbHER0Yr1t6eqEfTSaHJ4LzQ"}

	data=	{
	  "Sku": "VIP-47735",
	  "ImageUrls":[
	      "http://marketing.customprintcenter.com/GP/API/samples/cimpress.png"
	  ]
	}

	r = requests.post(url, data=data, headers=headers)
	# r = requests.post(url, data=json.dumps(data), headers=headers)
	print r.status_code, r.text
	# session = requests.Session()
	# print session.post(url, headers=headers, data=str(data))


def jwt():
	url = "https://cimpress.auth0.com/oauth/ro"
	headers = {'Content-Type': 'application/json'}
	data = {
				"client_id": "4GtkxJhz0U1bdggHMdaySAy05IV4MEDV",
				"username":"kushalvyaskv@gmail.com",
				"password":"pass@123",
				"connection":"default",
				"device":"none",
				"scope": "openid email app_metadata"
				}

	r = requests.post(url, data=json.dumps(data), headers=headers)
	print r.text
	print r.json()

def test_image_upload():

	url = "https://api.cimpress.io/vcs/printapi/v2/documents/creators/file"
	sku = "VIP-47735"
	encoded_image = None
	with open("/home/kushal/Pictures/homeboys.jpg", "rb") as image_file:
	    encoded_image = base64.b64encode(image_file.read())

	# print encoded_image[0]

	headers = {'Authorization' : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Imt1c2hhbHZ5YXNrdkBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InZjc19wYXJ0bmVyX2lkIjoiU3R1ZGVudEhhY2thdGhvbi03NTEyMWViYy0wYzVhLTQzM2UtOWViMi0wYTZkMzY0NjhhNzQifSwiaXNzIjoiaHR0cHM6Ly9jaW1wcmVzcy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg4OTk4MmQ1MDM1MWQzMmZmMzNjYjg5IiwiYXVkIjoiNEd0a3hKaHowVTFiZGdnSE1kYXlTQXkwNUlWNE1FRFYiLCJleHAiOjE0ODU2MzU1NjMsImlhdCI6MTQ4NTU5OTU2M30.JE0aNMBTUNfrTTPbcR8mbHER0Yr1t6eqEfTSaHJ4LzQ"}

	data=	{
	  "Sku": "VIP-47735",
	  "file": "data:image/png;base64,"+str(encoded_image)
	}

	# r = requests.post(url, files=json.dumps(data), headers=headers)
	r = requests.post(url, files=data, headers=headers)
	print r.status_code, r.text
	# session = requests.Session()

# jwt()
# test_image_url()
# test_image_upload()
# 
# 
import pyimgur
def imgure():
	CLIENT_ID = "b5269faedb893ff"
	PATH = "/home/kushal/KV/ihack/redhawk/media/case3.jpg"

	im = pyimgur.Imgur(CLIENT_ID)
	uploaded_image = im.upload_image(PATH, title="Uploaded with PyImgur")
	print(uploaded_image.title)
	print(uploaded_image.link)
	print(uploaded_image.size)
	print(uploaded_image.type)

# a = imgure()
# print a
# 
def scene_render():
	doc_url = "http://uds.documents.cimpress.io/v2/merchants/vcs/documents/StudentHackathon-76286f8e-a3db-4d2f-867a-ba535b6acfd7/docref?token=db93452309af54fec5dfb12d77f0b858"

	Sku = "VIP-47735"
	headers = {
		"Content-Type":"application/json",
		'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6Imt1c2hhbHZ5YXNrdkBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InZjc19wYXJ0bmVyX2lkIjoiU3R1ZGVudEhhY2thdGhvbi03NTEyMWViYy0wYzVhLTQzM2UtOWViMi0wYTZkMzY0NjhhNzQifSwiaXNzIjoiaHR0cHM6Ly9jaW1wcmVzcy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg4OTk4MmQ1MDM1MWQzMmZmMzNjYjg5IiwiYXVkIjoiNEd0a3hKaHowVTFiZGdnSE1kYXlTQXkwNUlWNE1FRFYiLCJleHAiOjE0ODU2NzU2NjcsImlhdCI6MTQ4NTYzOTY2N30.ZbY3G6at9hHcWAB2JZh3pN2cb4hqqY8GDqIiJaSerOo "
	}

	url = "https://api.cimpress.io/vcs/printapi/v2/documents/scenes?width=1000&documentReferenceUrl=http://uds.documents.cimpress.io/v2/merchants/vcs/documents/StudentHackathon-76286f8e-a3db-4d2f-867a-ba535b6acfd7/docref?token=db93452309af54fec5dfb12d77f0b858"

	r =  requests.get(url,headers=headers)
	data =  r.json()
	print data['Scenes'][0]['RenderingUrl']

scene_render()