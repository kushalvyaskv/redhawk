import json
import sys
from wit import Wit
from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response


# if len(sys.argv) != 2:
#     print('usage: python ' + sys.argv[0] + ' <wit-token>')
#     exit(1)
# access_token = sys.argv[1]

# Quickstart example
# See https://wit.ai/ar7hur/Quickstart
# 
class Joke(object):

    def __init__(self):
        self.call_method = None
        self.actions = {
                            'send': self.send,
                            'getForecast': self.get_forecast,
                            'getProducts' : self.getProducts,
                            'showMugs' : self.showMugs,
                            'mugImageUpload' : self.mugImageUpload,
                            'displayMugScene' : self.displayMugScene,
                            'mugOrderDetails' : self.mugOrderDetails,
                            'showTshirts' : self.showTshirts,
                            'tshirtImageUpload' : self.tshirtImageUpload,
                            'displayTshirtScene' : self.displayTshirtScene,
                            'tshirtOrderDetails' : self.tshirtOrderDetails,
                        }

        self.client  = Wit(access_token=settings.WIT_API_KEY, actions=self.actions)
        self.callback_responses = []
        self.http_req = None

    def first_entity_value(self, entities, entity):
        if entity not in entities:
            return None
        val = entities[entity][0]['value']
        if not val:
            return None
        return val['value'] if isinstance(val, dict) else val

    def send(self, request, response):
        self.callback_responses.append(response['text'])
        print(response['text'])

    def get_forecast(self,request):
        context = request['context']
        entities = request['entities']

        loc = self.first_entity_value(entities, 'location')
        if loc:
            context['forecast'] = 'sunny'
            if context.get('missingLocation') is not None:
                del context['missingLocation']
        else:
            context['missingLocation'] = True
            if context.get('forecast') is not None:
                del context['forecast']

        return context

    def getProducts(self, request):
        print "Show products is called"
        context = request['context']
        entities = request['entities']


        #Call API for displaying products 

        context['products'] = " "

        return context

    def execute_conversation(self, input_text, http_req):
        self.http_req = http_req
        self.callback_responses[:] = []
        context0= {}
        # self.only_reply = self.client.converse(1, input_text, {})
        # print self.only_reply
        self.reply = self.client.run_actions(1, input_text, context0)
        print self.reply, context0
        print "Callback response:", self.callback_responses
        return json.dumps(self.callback_responses)

    def interactive(self):
        self.client.interactive()


    # Mug Process
    def showMugs(self, request):
        print "request -> ", request
        context = request['context']
        print "entity" , request['entities']
        # entities = /request['entities']
        # API call to Show Mugs
        context['mugs_url'] = "mugs"

        self.call_method = "mug"
        context['showMugs'] = "Mugs Shown"
        return context

    def mugImageUpload(self, request):
        context = request['context']
        entities = request['entities']
        print "++++++++++++"
        print entities
        return context

    def displayMugScene(self, request):
        context = request['context']
        entities = request['entities']

        return context

    def mugOrderDetails(self, request):
        context = request['context']
        entities = request['entities']

        return context

    # Tshirt Process
    def showTshirts(self, request):
        print "request is ",request
        context = request['context']
        entities = request['entities']
        # API call to Show Mugs
        self.call_method = "tshirt"
        print "Tshirt function called"
        return context

    def tshirtImageUpload(self, request):
        context = request['context']
        entities = request['entities']
        print entities
        return context

    def displayTshirtScene(self, request):
        context = request['context']
        entities = request['entities']

        return context

    def tshirtOrderDetails(self, request):
        context = request['context']
        entities = request['entities']

        return context


# client = Wit(access_token=access_token, actions=actions)
# # client.interactive()

# while True:
#     input = raw_input()
#     if input == "xxx":
#         break;
#     else:
#         print client.run_actions(1,input,{})


    