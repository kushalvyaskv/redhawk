$url = 'https://api.cimpress.io/vcs/printapi/v2/documents/creators/file';
$data = array('Sku' => 'VIP-47735', 'ImageUrls' => ['http://marketing.customprintcenter.com/GP/API/samples/cimpress.png']);

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { /* Handle error */ }

var_dump($result);